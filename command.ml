type command =
  | Quit
  | Define of Formula.definition
  | Unroll
  | Prove of Deduction.sequent
  | ApplyRule of Deduction.rule
  | HelpOp of Formula.operator
  | HelpElim
  | HelpIntro
  | HelpClassical
  | Help
  | Undo
  | Auto
  | Print
  | French
  | LaTeX
  | FrenchLaTeX
  | Pdf
  | Qed
